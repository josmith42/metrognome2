use std::{path::{Path, PathBuf}, env, fs::copy};

const LIB_CPP_SHARED: &str = "libc++_shared.so";
const ANDROID_SDK_ROOT: &str = "ANDROID_SDK_ROOT";

const NDK_VERSION: &str = "21.3.6528147";

fn main() {
    println!("cargo:rustc-link-lib=dylib=c++_shared");

    let cppsharedlib = cppsharedlib_path();
    let out_dir = get_output_path();
    let out_path = Path::new(&out_dir).join(LIB_CPP_SHARED);
    copy(cppsharedlib, out_path).unwrap();
}

fn get_output_path() -> PathBuf {
    //<root or manifest path>/target/<profile>/
    let manifest_dir_string = env::var("CARGO_MANIFEST_DIR").unwrap();
    let build_type = env::var("PROFILE").unwrap();
    let target = env::var("TARGET").unwrap();
    Path::new(&manifest_dir_string).join("target").join(target).join(build_type)
}

fn cppsharedlib_path() -> String {
    let base_path = ndk_base_path();
    let target = env::var("TARGET").unwrap();
    format!("{base_path}/lib/{target}/{LIB_CPP_SHARED}")
}

fn ndk_base_path() -> String {
    let android_sdk_root = env::var(ANDROID_SDK_ROOT).unwrap_or_else(|_| panic!("{ANDROID_SDK_ROOT} environment variable must be defined"));

    let build_platform = ndk_build_platform();

    let base_path = format!("{android_sdk_root}/ndk/{NDK_VERSION}/toolchains/llvm/prebuilt/{build_platform}-x86_64/sysroot/usr");
    assert!(Path::new(&base_path).is_dir(), "Path `{base_path}` does not exist. Maybe check the value of {ANDROID_SDK_ROOT} (`{android_sdk_root}`)");
    base_path
}

fn ndk_build_platform() -> &'static str {
    if cfg!(target_os = "macos") {
        "darwin"
    } else if cfg!(target_os = "linux") {
        "linux"
    } else {
        panic!("Unsupported build platform");
    }
}