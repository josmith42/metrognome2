use ndk::asset::AssetManager;
use jni::sys::{JNIEnv, jobject};
use ndk_sys::{self, AAssetManager};
use std::ptr::NonNull;

pub unsafe fn to_native_asset_manager(env: *mut JNIEnv, assetManager: jobject) -> AssetManager {
    unsafe {
        let am_ptr = ndk_sys::AAssetManager_fromJava(env, assetManager) as *mut AAssetManager;
        let am_ptr = NonNull::new(am_ptr).unwrap_or_else(|| { panic!("Expected AAssetManager pointer to be non-null!") });
        AssetManager::from_ptr(am_ptr)
    }
}