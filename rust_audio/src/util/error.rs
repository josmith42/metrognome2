use std::error::Error;
use std::fmt::{Display, Formatter};

#[derive(Debug)]
pub struct MgError {
    msg: String
}

impl MgError {
    // pub fn new<T>(msg: &str) -> Result<T> {
    //     Err(Box::new(MgError { msg: String::from(msg) }))
    // }
}

impl Error for MgError {}

impl Display for MgError {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        write!(f, "{}", self.msg)
    }
}

pub type Result<T> = std::result::Result<T, Box<dyn Error>>;
