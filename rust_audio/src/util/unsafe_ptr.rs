use std::ops::Deref;
use std::ops::DerefMut;

#[derive(Debug)]
pub struct UnsafePtr<T> where T: ?Sized {
    ptr: * mut T
}

impl <T> UnsafePtr<T> {
    pub fn new(t: T) -> Self {
        Self { ptr: Box::leak(Box::new(t)) }
    }

    pub unsafe fn from_raw(raw: u64) -> Option<UnsafePtr<T>> {
        if raw != 0 { Some(UnsafePtr { ptr: raw as *mut T }) }
        else { None }
    }

    pub fn into_raw(self) -> u64 {
        self.ptr as u64
    }

    pub fn destroy(self) -> Box<T> {
        unsafe { Box::from_raw(self.ptr) }
    }
}

impl<T> Deref for UnsafePtr<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        unsafe { &*self.ptr }
    }
}
impl<T> DerefMut for UnsafePtr<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { &mut *self.ptr }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn pointer_from_raw_works_ok() {
        let p = UnsafePtr::new(42);
        let i = p.into_raw();
        let p = unsafe { UnsafePtr::<i32>::from_raw(i) };
        assert_eq!(42, *p.unwrap());
    }

    #[test]
    fn pointer_from_null_raw_returns_none() {
        let p = unsafe { UnsafePtr::<i32>::from_raw(0) };
        assert!(p.is_none());
    }
}