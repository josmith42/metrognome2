#![cfg(target_os = "android")]
#![allow(non_snake_case)]

mod audio;
mod android;
mod util;

use std::result::Result::Err;

use android_logger::Config;
use log::{LevelFilter, error};
use jni::objects::JClass;
use jni::sys::{JNIEnv, jlong, jint, jobject};
use audio::audio_engine::AudioEngine;
use audio::audio_data_loader::AudioDataLoader;
use util::unsafe_ptr::UnsafePtr;

#[no_mangle]
pub extern "C" fn Java_com_josmith42_metrognome2_native_NativeAudio_initLogging(
    _: *mut JNIEnv,
    _: JClass,
) {
    android_logger::init_once(
        Config::default()
            .with_max_level(LevelFilter::Trace)
            .with_tag("metrognome"),
    );
}

/// Initializes the audio stream.
/// * `env` - java environment
/// * `assetManager` - used to retrieve the wav assets
/// 
/// # Safety
/// The `env` and `assetManager` objects must be valid and non-null. This will be the
/// case if this method is called correctly from java/kotlin.
#[no_mangle]
pub unsafe extern "C" fn Java_com_josmith42_metrognome2_native_NativeAudio_initAudio(
    env: *mut JNIEnv,
    _: JClass,
    assetManager: jobject
) -> jlong {
    let assetManager = unsafe { android::to_native_asset_manager(env, assetManager) };
    let audioDataLoader = AudioDataLoader::new(assetManager);

    match AudioEngine::new(&audioDataLoader) {
        Ok(audio_engine) => UnsafePtr::new(audio_engine).into_raw() as jlong,
        Err(msg) => {
            error!("Failed to start audio engine: {}", msg);
            0
        }
    }
}

#[no_mangle]
pub extern "C" fn Java_com_josmith42_metrognome2_native_NativeAudio_setTempo(
    _: *mut JNIEnv,
    _: JClass,
    audio_engine_ptr: jlong,
    tempo: jint
) {
    if let Some(audio_engine) = unsafe { UnsafePtr::<AudioEngine>::from_raw(audio_engine_ptr as u64) } {
        if let Err(msg) = audio_engine.set_tempo(tempo) {
            error!("Failed to set tempo: {msg}");
        }
    }
}

#[no_mangle]
pub extern "C" fn Java_com_josmith42_metrognome2_native_NativeAudio_startPlaying(
    _: *mut JNIEnv,
    _: JClass,
    audio_engine_ptr: jlong
) {
    if let Some(audio_engine) = unsafe { UnsafePtr::<AudioEngine>::from_raw(audio_engine_ptr as u64) } {
        if let Err(msg) = audio_engine.start() {
            error!("Failed to start: {msg}");
        }
    }
}

#[no_mangle]
pub extern "C" fn Java_com_josmith42_metrognome2_native_NativeAudio_stopPlaying(
    _: *mut JNIEnv,
    _: JClass,
    audio_engine_ptr: jlong
) {
    if let Some(audio_engine) = unsafe { UnsafePtr::<AudioEngine>::from_raw(audio_engine_ptr as u64) } {
        if let Err(msg) = audio_engine.stop() {
            error!("Failed to stop: {}", msg);
        }
    }
}

#[no_mangle]
pub extern "C" fn Java_com_josmith42_metrognome2_native_NativeAudio_destroy(
    _: *mut JNIEnv,
    _: JClass,
    audio_engine_ptr: jlong
) {
    if let Some(audio_engine) = unsafe { UnsafePtr::<AudioEngine>::from_raw(audio_engine_ptr as u64) } {
        audio_engine.destroy();
    }
}