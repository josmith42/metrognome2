
pub struct FrameGenerator {
    playing: bool,
    frame_rate: i32,
    frame_interval: usize,
    frame_interval_index: usize,
    sound_data: Vec<f32>,
}

impl FrameGenerator {
    pub fn new() -> Self {
        Self {
            playing: false,
            frame_rate: 0,
            frame_interval: 0,
            sound_data: Vec::new(),
            frame_interval_index: 0,
        }
    }

    pub fn next_frame(&mut self) -> f32 {
        if self.playing && !self.sound_data.is_empty() {
            if self.frame_interval_index >= self.frame_interval {
                self.frame_interval_index = 0
            }

            let frame = if self.frame_interval_index < self.sound_data.len() {
                self.sound_data[self.frame_interval_index]
            } else {
                0f32
            };
            self.frame_interval_index += 1;

            frame
        } else {
            0f32
        }
    }

    pub fn start(&mut self) {
        self.frame_interval_index = 0;
        self.playing = true;
    }

    pub fn stop(&mut self) {
        self.playing = false;
    }

    pub fn set_frame_rate(&mut self, frame_rate: i32) {
        self.frame_rate = frame_rate;
    }

    pub fn set_sound(&mut self, mut sound_data: Vec<f32>) {
        std::mem::swap(&mut self.sound_data, &mut sound_data);
    }

    pub fn set_tempo(&mut self, tempo: i32) {
        let intervals_per_sec = 60f32 / tempo as f32;
        self.frame_interval = (intervals_per_sec * self.frame_rate as f32) as usize;
    }
}