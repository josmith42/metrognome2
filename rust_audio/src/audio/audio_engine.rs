
use std::sync::mpsc::{sync_channel, SyncSender};

use oboe::{
    AudioStream, AudioStreamAsync, AudioStreamBase, AudioStreamBuilder,
    Mono, Output, PerformanceMode, SharingMode,
};

use super::audio_generator::AudioGenerator;
use super::audio_data_loader::AudioDataLoader;
use crate::util::error::Result;

pub enum Command {
    Start,
    Stop,
    SetFrameRate(i32),
    SetSound(Vec<f32>),
    SetTempo(i32)
}

/// Sine-wave generator stream
pub struct AudioEngine {
    stream : AudioStreamAsync<Output, AudioGenerator>,
    command_sender: SyncSender<Command>
}

impl AudioEngine {
    pub fn new(audioDataLoader: &AudioDataLoader) -> Result<AudioEngine> {
        let (command_sender, receive) = sync_channel(128);

        let mut stream = AudioStreamBuilder::default()
            .set_performance_mode(PerformanceMode::LowLatency)
            .set_sharing_mode(SharingMode::Shared)
            .set_format::<f32>()
            .set_channel_count::<Mono>()
            .set_callback(AudioGenerator::new(receive))
            .open_stream()?;

        log::debug!("start stream: {:?}", stream);

        stream.start()?;

        let engine = Self { stream, command_sender };

        engine.command_sender.send(Command::SetFrameRate(engine.stream.get_sample_rate()))?;

        let sound_name = "beep.wav";
        let sound_data = audioDataLoader.load(sound_name)?;
        log::debug!("set_sound: {sound_name} (size: {})", sound_data.len());
        engine.command_sender.send(Command::SetSound(sound_data))?;

        Ok(engine)
    }

    pub fn set_tempo(&self, tempo: i32) -> Result<()> {
        log::debug!("set_tempo({tempo})");
        Ok(self.command_sender.send(Command::SetTempo(tempo))?)
    }

    pub fn start(&self) -> Result<()> {
        log::debug!("start()");
        Ok(self.command_sender.send(Command::Start)?)
    }

    pub fn stop(&self) -> Result<()> {
        log::debug!("stop()");
        Ok(self.command_sender.send(Command::Stop)?)
    }
}

impl Drop for AudioEngine {
    fn drop(&mut self) {
        log::debug!("stop stream: {:?}", self.stream);
        if let Err(err) = self.stream.stop() {
            log::error!("Error while stopping stream: {}", err);
        }
    }
}