use std::{
    sync::{
        mpsc::Receiver
    },
};

use oboe::{
    AudioOutputCallback,
    AudioOutputStreamSafe, 
    DataCallbackResult, Mono
};

use super::audio_engine::Command;
use super::frame_generator::FrameGenerator;

pub struct AudioGenerator {
    frame_generator: FrameGenerator,
    command_receiver: Receiver<Command>
}

impl AudioGenerator {
    pub fn new(command_receiver: Receiver<Command>) -> Self {
        Self {
            frame_generator: FrameGenerator::new(),
            command_receiver
        }
    }

    fn process_command(&mut self) {
        match self.command_receiver.try_recv() {
            Ok(Command::Start) => self.frame_generator.start(),
            Ok(Command::Stop) => self.frame_generator.stop(),
            Ok(Command::SetFrameRate(frame_rate)) => self.frame_generator.set_frame_rate(frame_rate),
            Ok(Command::SetSound(sound_data)) => self.frame_generator.set_sound(sound_data),
            Ok(Command::SetTempo(tempo)) => self.frame_generator.set_tempo(tempo),
            Err(_) => {}
        }
    }
}

impl AudioOutputCallback for AudioGenerator {
    type FrameType = (f32, Mono);

    fn on_audio_ready(
        &mut self,
        _stream: &mut dyn AudioOutputStreamSafe,
        frames: &mut [f32],
    ) -> DataCallbackResult {
        for frame in frames {
            self.process_command();
            *frame = self.frame_generator.next_frame()
        }
        DataCallbackResult::Continue
    }
}