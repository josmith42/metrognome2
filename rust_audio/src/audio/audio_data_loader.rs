use std::ffi::CString;
use std::io::Read;

use ndk::asset::AssetManager;
use wav_io::reader::Reader;

use crate::util::error::Result;

pub struct AudioDataLoader {
    assetManager: AssetManager
}

impl AudioDataLoader {
    pub fn new(assetManager: AssetManager) -> Self {
        Self { assetManager }
    }

    pub fn load(&self, fileName: &str) -> Result<Vec<f32>> {
        let fileName = CString::new(fileName)?;
        let mut asset = self.assetManager.open(&fileName).ok_or("Cannot open asset")?;
        let mut data = Vec::new();
        asset.read_to_end(&mut data)?;
        log::debug!("raw audio data size: {}", data.len());

        let mut wav_reader = Reader::from_vec(data)?;

        // Calling read_header() is necessary for get_samples_f32() to work.
        wav_reader.read_header()?;

        Ok(wav_reader.get_samples_f32()?)
    }
}