[ -z "$ANDROID_NDK_VERSION" ] && echo "ANDROID_NDK_VERSION MUST be set" >&2 && exit 1
[ -z "$ANDROID_SDK_ROOT" ] && echo "ANDROID_SDK_ROOT MUST be set" >&2 && exit 1

case $(uname) in
  Linux)
    BUILD_MACHINE=linux-x86_64
  ;;
  Darwin)
    BUILD_MACHINE=darwin-x86_64
  ;;
  *)
    echo "Unknown build machine type: $(uname)" >&2
    exit 1
  ;;
esac

CLANG_TOOLCHAIN="${ANDROID_SDK_ROOT}/ndk/${ANDROID_NDK_VERSION}/toolchains/llvm/prebuilt/$BUILD_MACHINE/bin"
RUST_LIB=rust_audio/.cargo
CONFIG_TOML="$RUST_LIB/config.toml"

mkdir -p $RUST_LIB

cat << EOF > $CONFIG_TOML
[target.aarch64-linux-android]
linker = "${CLANG_TOOLCHAIN}/aarch64-linux-android21-clang++"
ar = "${CLANG_TOOLCHAIN}/aarch64-linux-android-ar"

[target.x86_64-linux-android]
linker = "${CLANG_TOOLCHAIN}/x86_64-linux-android21-clang++"
ar = "${CLANG_TOOLCHAIN}/x86_64-linux-android-ar"
EOF

echo Wrote the following to \"$CONFIG_TOML\":
cat $CONFIG_TOML