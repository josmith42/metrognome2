package com.josmith42.metrognome2.viewmodel

import com.josmith42.metrognome2.controller.AudioController
import com.josmith42.metrognome2.controller.AudioControllerProvider
import io.mockk.*
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
internal class MetronomeViewModelTest {

    class AudioControllerProviderStub(
        private val audioController: AudioController
    ) : AudioControllerProvider {
        override fun provide(): AudioController = audioController
        override fun destroy() {}
    }

    private fun makeMockAudioController() : AudioController {
        val audioController: AudioController = mockk()
        every { audioController.isInitialized } returns true
        every { audioController.setTempo(any()) } just runs
        every { audioController.start() } just runs
        every { audioController.stop() } just runs
        return audioController
    }
    private fun makeViewModel(audioController: AudioController? = null) =
        MetronomeViewModel(AudioControllerProviderStub(audioController ?: AudioControllerStub()))

    @Test
    fun `togglePlaying sets playing to true`() {
        val audioController = makeMockAudioController()
        val metronomeViewModel = makeViewModel(audioController)

        metronomeViewModel.togglePlaying()

        assertTrue(metronomeViewModel.isPlaying.value!!)
        verify { audioController.start() }
    }

    @Test
    fun `togglePlaying twice sets playing to false`() {
        val audioController = makeMockAudioController()
        val metronomeViewModel = makeViewModel(audioController)

        metronomeViewModel.togglePlaying()
        metronomeViewModel.togglePlaying()

        assertFalse(metronomeViewModel.isPlaying.value!!)
        verify { audioController.stop() }
    }

    class AudioControllerStub : AudioController {
        override val isInitialized = true
        override fun start() {}
        override fun stop() {}
        override fun setTempo(tempo: Int) {}
    }


    @Test
    fun `onTempoSet sets tempo live data`() {
        val metronomeViewModel = makeViewModel()

        assertEquals(120, metronomeViewModel.tempo.value)

        metronomeViewModel.onTempoSet("42")
        assertEquals(42, metronomeViewModel.tempo.value)
    }

    @Test
    fun `onTempoSet invalid data does not set tempo live data`() {
        val metronomeViewModel = makeViewModel()

        metronomeViewModel.onTempoSet("42")

        metronomeViewModel.onTempoSet("")
        assertEquals(42, metronomeViewModel.tempo.value)

        metronomeViewModel.onTempoSet("39")
        assertEquals(42, metronomeViewModel.tempo.value)

        metronomeViewModel.onTempoSet("209")
        assertEquals(42, metronomeViewModel.tempo.value)

        metronomeViewModel.onTempoSet("a")
        assertEquals(42, metronomeViewModel.tempo.value)
    }

    @Test
    fun `isTempoValid returns correct values`() {
        val metronomeViewModel = makeViewModel()

        assertTrue(metronomeViewModel.isTempoValid("42"))
        assertTrue(metronomeViewModel.isTempoValid("40"))
        assertTrue(metronomeViewModel.isTempoValid("208"))

        assertFalse(metronomeViewModel.isTempoValid("39"))
        assertFalse(metronomeViewModel.isTempoValid("209"))
        assertFalse(metronomeViewModel.isTempoValid(""))
        assertFalse(metronomeViewModel.isTempoValid("a"))
    }
}