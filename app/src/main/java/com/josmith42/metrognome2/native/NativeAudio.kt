package com.josmith42.metrognome2.native

import android.content.res.AssetManager

class NativeAudio {
    companion object {
        init {
            System.loadLibrary("c++_shared")
            System.loadLibrary("rust_audio")
            initLogging()
        }

        @JvmStatic
        external fun initLogging()

        @JvmStatic external fun initAudio(assetManager: AssetManager): Long

        @JvmStatic
        external fun setTempo(audio_engine_ptr: Long, tempo: Int)

        @JvmStatic
        external fun startPlaying(audio_engine_ptr: Long)

        @JvmStatic
        external fun stopPlaying(audio_engine_ptr: Long)

        @JvmStatic
        external fun destroy(audio_engine_ptr: Long)
    }
}