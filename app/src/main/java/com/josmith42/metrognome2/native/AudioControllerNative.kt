package com.josmith42.metrognome2.native

import com.josmith42.metrognome2.controller.AudioController
import javax.inject.Inject

class AudioControllerNative @Inject constructor(
    private val nativeAudioPtr: Long = 0
) : AudioController {

    override val isInitialized: Boolean
        get() = nativeAudioPtr != 0L

    override fun start() {
        NativeAudio.startPlaying(nativeAudioPtr)
    }

    override fun stop() {
        NativeAudio.stopPlaying(nativeAudioPtr)
    }

    override fun setTempo(tempo: Int) {
        NativeAudio.setTempo(nativeAudioPtr, tempo)
    }

    fun destroy() {
        NativeAudio.destroy(nativeAudioPtr)
    }
}