package com.josmith42.metrognome2.controller

import android.content.res.AssetManager
import com.josmith42.metrognome2.native.AudioControllerNative
import com.josmith42.metrognome2.native.NativeAudio
import javax.inject.Singleton

@Singleton
class AudioControllerProviderImpl(
    private val assetManager: AssetManager
) : AudioControllerProvider {
    private var audioController: AudioControllerNative? = null

    override fun provide() : AudioController = audioController ?: makeAudioController()

    private fun makeAudioController(): AudioController {
        val nativePtr = NativeAudio.initAudio(assetManager)
        return AudioControllerNative(nativePtr).also {
            audioController = it
        }
    }

    override fun destroy() {
        audioController?.destroy()
        audioController = null
    }
}