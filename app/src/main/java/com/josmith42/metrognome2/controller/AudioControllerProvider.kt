package com.josmith42.metrognome2.controller

interface AudioControllerProvider {
    fun provide() : AudioController
    fun destroy()
}