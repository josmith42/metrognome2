package com.josmith42.metrognome2.controller

interface AudioController {
    val isInitialized: Boolean

    fun start()
    fun stop()
    fun setTempo(tempo: Int)
}