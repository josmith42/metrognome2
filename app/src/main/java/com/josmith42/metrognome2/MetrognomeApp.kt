package com.josmith42.metrognome2

import android.app.Application
import android.util.Log
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MetrognomeApp: Application()