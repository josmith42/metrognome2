package com.josmith42.metrognome2

import android.app.Activity
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import com.josmith42.metrognome2.controller.AudioControllerProvider
import dagger.hilt.android.scopes.ActivityRetainedScoped
import javax.inject.Inject

val LifecycleOwner.isChangingConfigurations: Boolean
    get() = if (this is Activity) this.isChangingConfigurations
    else false

@ActivityRetainedScoped
class MainActivityListener @Inject constructor(
    private val audioControllerProvider: AudioControllerProvider
): DefaultLifecycleObserver {
    override fun onStart(owner: LifecycleOwner) {
        audioControllerProvider.provide()
    }

    override fun onStop(owner: LifecycleOwner) {
        if (!owner.isChangingConfigurations) {
            audioControllerProvider.destroy()
        }
    }
}