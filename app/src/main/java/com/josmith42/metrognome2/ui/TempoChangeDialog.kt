package com.josmith42.metrognome2.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import com.josmith42.metrognome2.ui.theme.Typography
import com.josmith42.metrognome2.viewmodel.MAX_TEMPO
import com.josmith42.metrognome2.viewmodel.MIN_TEMPO
import kotlinx.coroutines.delay

@Composable
internal fun TempoChangeDialog(
    tempo: Int,
    onValidateTempo: (String) -> Boolean,
    onTempoChange: (String) -> Unit,
    onDismiss: () -> Unit
) {
    val textFieldState = remember {
        val tempoText = tempo.toString()
        mutableStateOf(
            TextFieldValue(
                text = tempoText,
                selection = TextRange(0, tempoText.length)
            )
        )
    }
    var enteredTempoValid by remember { mutableStateOf(true) }

    val focusRequester = remember { FocusRequester() }

    val onOk = {
        if (enteredTempoValid) {
            onTempoChange(textFieldState.value.text)
            onDismiss()
        }
    }

    Dialog(
        onDismissRequest = onDismiss,
    ) {
        Column(
            modifier = Modifier
                .background(color = MaterialTheme.colors.background)
                .padding(24.dp)
        ) {
            Text("Enter a value between $MIN_TEMPO and $MAX_TEMPO")
            TextField(
                value = textFieldState.value,
                isError = !enteredTempoValid,
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Number,
                ),
                keyboardActions = KeyboardActions(
                    onDone = { onOk() }
                ),
                singleLine = true,
                textStyle = Typography.h2,
                onValueChange = { enteredValue ->
                    textFieldState.value = enteredValue
                    enteredTempoValid = onValidateTempo(enteredValue.text)
                },
                modifier = Modifier
                    .focusRequester(focusRequester)
            )
            Text(
                text = if (enteredTempoValid) "" else "Invalid Value",
                color = MaterialTheme.colors.error
            )
            Button(
                modifier = Modifier.align(Alignment.End),
                onClick = onOk,
                enabled = enteredTempoValid
            ) {
                Text("OK")
            }
        }

        LaunchedEffect(Unit) {
            // HACK - this delay is necessary to allow the soft keyboard to display when the
            // TextField gets focus, for some reason. Without the delay, the TextField gets focus,
            // but the soft keyboard doesn't display. This is likely to be a flaky solution, but I
            // haven't found a better one yet that actually works.
            delay(300)
            focusRequester.requestFocus()
        }
    }
}

@Composable
@Preview
fun PreviewChangeTempoDialog() {
    TempoChangeDialog(tempo = 120, onValidateTempo = { true }, onTempoChange = {}) { }
}