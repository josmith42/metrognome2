package com.josmith42.metrognome2.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val StartGreenDark = Color(0xFF2D6B2D)
val StopRedDark = Color(0xFFBE1616)

val StartGreenLight = Color(0xFF5cb85c)
val StopRedLight = Color(0xffd9534f)
val playText = Color(0xFFFFFFFF)
