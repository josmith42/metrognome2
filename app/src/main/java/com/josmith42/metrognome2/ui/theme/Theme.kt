package com.josmith42.metrognome2.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.Immutable
import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.graphics.Color

private val DarkColorPalette = darkColors(
    primary = Purple200,
    primaryVariant = Purple700,
    secondary = Teal200
)

private val LightColorPalette = lightColors(
    primary = Purple500,
    primaryVariant = Purple700,
    secondary = Teal200

    /* Other default colors to override
    background = Color.White,
    surface = Color.White,
    onPrimary = Color.White,
    onSecondary = Color.Black,
    onBackground = Color.Black,
    onSurface = Color.Black,
    */
)

@Immutable
data class ExtendedColors(
    val playButton: Color,
    val stopButton: Color,
    val playButtonText: Color
)

val LocalExtendedColors = staticCompositionLocalOf {
    ExtendedColors(
        playButton = Color.Unspecified,
        stopButton = Color.Unspecified,
        playButtonText = Color.Unspecified
    )
}

@Composable
fun Metrognome2Theme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    val extendedColors = if (darkTheme) ExtendedColors(
        playButton = StartGreenDark,
        stopButton = StopRedDark,
        playButtonText = playText
    ) else ExtendedColors(
        playButton = StartGreenLight,
        stopButton = StopRedLight,
        playButtonText = playText
    )

    CompositionLocalProvider(LocalExtendedColors provides extendedColors) {
        MaterialTheme(
            colors = colors,
            shapes = Shapes,
            typography = Typography,
            content = content
        )
    }
}

object Metrognome2Theme {
    val colors: ExtendedColors
        @Composable
        get() = LocalExtendedColors.current
}