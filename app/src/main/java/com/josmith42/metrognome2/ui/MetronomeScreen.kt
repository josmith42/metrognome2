package com.josmith42.metrognome2.ui

import android.view.MotionEvent
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Alignment.Companion.End
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.pointer.pointerInteropFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.lifecycle.viewmodel.compose.viewModel
import com.josmith42.metrognome2.R
import com.josmith42.metrognome2.ui.theme.Metrognome2Theme
import com.josmith42.metrognome2.ui.theme.Typography
import com.josmith42.metrognome2.viewmodel.MetronomeViewModel

@Composable
fun MetronomeScreen() {
    val metronomeViewModel: MetronomeViewModel = viewModel()
    val isPlaying: Boolean? by metronomeViewModel.isPlaying.observeAsState()
    val tempo: Int? by metronomeViewModel.tempo.observeAsState()
    val errorMessage: String? by metronomeViewModel.errorMessage.observeAsState()
    MetronomeComponent(
        isPlaying = isPlaying ?: false,
        tempo = tempo ?: 0,
        errorMessage = errorMessage,
        onValidateTempo = { metronomeViewModel.isTempoValid(it) },
        onTempoChange = { metronomeViewModel.onTempoSet(it) },
        onStartStop = { metronomeViewModel.togglePlaying() },
        onDismissError = { metronomeViewModel.dismissError() }
    )
}

@Composable
fun MetronomeComponent(
    isPlaying: Boolean,
    tempo: Int,
    errorMessage: String?,
    onValidateTempo: (String) -> Boolean,
    onTempoChange: (String) -> Unit,
    onStartStop: () -> Unit,
    onDismissError: () -> Unit
) {
    Box(modifier = Modifier.fillMaxSize()) {
        TempoDisplay(
            modifier = Modifier.align(Alignment.TopCenter),
            tempo = tempo,
            onValidateTempo = onValidateTempo,
            onTempoChange = onTempoChange
        )
        PlayButton(
            modifier = Modifier.align(Alignment.BottomCenter),
            isPlaying = isPlaying,
            onClick = onStartStop
        )
    }
    if (errorMessage != null) {
        AudioErrorDialog(errorMessage, onDismissError)
    }
}

// @OptIn needed because of use of TextField
@Composable
private fun TempoDisplay(
    modifier: Modifier,
    tempo: Int,
    onValidateTempo: (String) -> Boolean,
    onTempoChange: (String) -> Unit,
) {
    var dialogOpen by remember { mutableStateOf(false) }

    Column(
        modifier = modifier
            .padding(24.dp)
            .fillMaxWidth()
            .clickable { dialogOpen = true }
    ) {
        Text(
            modifier = Modifier
                .align(CenterHorizontally),
            style = Typography.h1,
            text = tempo.toString()
        )
        Text(
            modifier = Modifier
                .align(CenterHorizontally)
                .padding(bottom = 24.dp),
            text = stringResource(R.string.tap_to_change)
        )
    }

    if (dialogOpen) {
        TempoChangeDialog(
            tempo,
            onValidateTempo,
            onTempoChange,
            onDismiss = { dialogOpen = false })
    }
}


// @OptIn annotation needed for pointerInteropFilter
@Composable
@OptIn(ExperimentalComposeUiApi::class)
private fun PlayButton(modifier: Modifier, isPlaying: Boolean, onClick: () -> Unit) {
    val (buttonColor, buttonText, buttonIcon) =
        if (isPlaying) {
            Triple(
                Metrognome2Theme.colors.stopButton,
                R.string.stop,
                R.drawable.ic_stop_white_24dp
            )
        } else {
            Triple(
                Metrognome2Theme.colors.playButton,
                R.string.start,
                R.drawable.ic_play_arrow_white_24dp
            )
        }
    Button(
        modifier = modifier
            .fillMaxWidth()
            .padding(8.dp)
            .pointerInteropFilter {
                if (it.action == MotionEvent.ACTION_DOWN) {
                    onClick()
                    true
                } else false
            },
        onClick = {},
        colors = ButtonDefaults.buttonColors(
            backgroundColor = buttonColor,
            contentColor = Metrognome2Theme.colors.playButtonText
        )
    ) {
        Box(modifier = Modifier.fillMaxWidth()) {
            Icon(
                modifier = Modifier
                    .align(Alignment.CenterStart)
                    .padding(top = 2.dp),
                painter = painterResource(id = buttonIcon),
                contentDescription = null
            )
            Text(
                modifier = Modifier.align(Alignment.Center),
                text = stringResource(id = buttonText)
            )
        }
    }
}

@Composable
fun AudioErrorDialog(
    errorMessage: String,
    onDismissError: () -> Unit
) {
    Dialog(
        onDismissRequest = onDismissError
    ) {
        Column {
            Text(errorMessage)
            Button(
                modifier = Modifier.align(End),
                onClick = onDismissError
            ) {
                Text("OK")
            }
        }
    }
}

@Composable
@Preview(showSystemUi = true)
fun MetronomePreview() {
    MetronomeComponent(
        isPlaying = false,
        tempo = 120,
        errorMessage = null,
        onValidateTempo = { true },
        onTempoChange = {},
        onStartStop = {},
        onDismissError = {}
    )
}

@Composable
@Preview(showSystemUi = true)
fun MetronomeErrorPreview() {
    MetronomeComponent(
        isPlaying = false,
        tempo = 120,
        errorMessage = "Something went wrong",
        onValidateTempo = { true },
        onTempoChange = {},
        onStartStop = {},
        onDismissError = {}
    )
}