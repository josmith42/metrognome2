package com.josmith42.metrognome2.modules

import android.content.res.AssetManager
import com.josmith42.metrognome2.controller.AudioController
import com.josmith42.metrognome2.controller.AudioControllerProvider
import com.josmith42.metrognome2.controller.AudioControllerProviderImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AudioModule {
    @Provides
    @Singleton
    fun provideAudioControllerProvider(assetManager: AssetManager): AudioControllerProvider =
        AudioControllerProviderImpl(assetManager)

    @Provides
    fun provideAudioController(audioControllerProvider: AudioControllerProvider): AudioController =
        audioControllerProvider.provide()
}