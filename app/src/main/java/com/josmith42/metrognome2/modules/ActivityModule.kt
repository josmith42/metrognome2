package com.josmith42.metrognome2.modules

import com.josmith42.metrognome2.MainActivityListener
import com.josmith42.metrognome2.controller.AudioControllerProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped

@Module
@InstallIn(ActivityRetainedComponent::class)
object ActivityModule {
    @Provides
    @ActivityRetainedScoped
    fun provideMainActivityListener(audioControllerProvider: AudioControllerProvider) =
        MainActivityListener(audioControllerProvider)
}