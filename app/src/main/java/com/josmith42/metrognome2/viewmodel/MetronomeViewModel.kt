package com.josmith42.metrognome2.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.josmith42.metrognome2.controller.AudioController
import com.josmith42.metrognome2.controller.AudioControllerProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

const val MIN_TEMPO = 40
const val MAX_TEMPO = 208

@HiltViewModel
class MetronomeViewModel @Inject constructor(
    private val audioControllerProvider: AudioControllerProvider
) : ViewModel() {
    private val audioController: AudioController
        get() = audioControllerProvider.provide()

    private var _isPlaying = false
    private val _isPlayingLiveData = MutableLiveData(false)
    val isPlaying: LiveData<Boolean> = _isPlayingLiveData

    private val _tempoLiveData = MutableLiveData<Int>()
    val tempo: LiveData<Int> = _tempoLiveData

    private val _errorMessage = MutableLiveData<String>(null)
    val errorMessage: LiveData<String> = _errorMessage

    init {
        if (!audioController.isInitialized) {
            _errorMessage.value = "There was an error initializing audio."
        }
        onTempoSet("120")
    }

    fun togglePlaying() {
        _isPlaying = !_isPlaying
        if (_isPlaying) {
            audioController.start()
        } else {
            audioController.stop()
        }
        _isPlayingLiveData.value = _isPlaying
    }

    fun isTempoValid(userEnteredTempo: String) = parseTempoAndValidate(userEnteredTempo) != null

    fun onTempoSet(userEnteredTempo: String) {
        val newTempo = parseTempoAndValidate(userEnteredTempo)
        if (newTempo != null) {
            audioController.setTempo(newTempo)
            _tempoLiveData.value = newTempo
        }
    }

    fun dismissError() {
        _errorMessage.value = null
    }

    private fun parseTempoAndValidate(userEnteredTempo: String) =
        userEnteredTempo.toIntOrNull()?.let {
            if (it in MIN_TEMPO..MAX_TEMPO) it else null
        }
}